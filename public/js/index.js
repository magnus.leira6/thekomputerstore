
let bankBalanceEl = document.getElementById("bankBalance");
let outstandingLoanEl = document.getElementById("outstandingLoan");
let payEl = document.getElementById("pay");
const bankButtonEl = document.getElementById("bankButton");
const salaryToBankButtonEl = document.getElementById("salaryToBankButton");
const salaryButtonEl = document.getElementById("salaryButton");
const salaryToLoanButtonEl = document.getElementById("salaryToLoanButton");
const laptopSelectorEl = document.getElementById("laptopSelector");
const laptopDescriptionEl = document.getElementById("laptopDescription");
const laptopInformationEl = document.getElementById("laptopInformationBox");
const laptopImageEl = document.getElementById("laptopImage");
const laptopInformationDescriptionEl = document.getElementById("laptopInformationDescription");
const laptopInformationTitleEl = document.getElementById("laptopInformationTitle");
const laptopPriceEl = document.getElementById("laptopPrice");
const buyButtomEl = document.getElementById("buyButton");

const converter = new Intl.NumberFormat("nb-NO", { style: "currency", currency: 'NOK' });
bankBalanceEl.innerText = converter.format(200);
const laptops = [];


function fromNOKToNumber(num) {
    let numb = num.split(",")
    console.log(numb);
    return parseInt(numb[0].replace(/\D/g, ""));
}

//Handles loaning of money
bankButtonEl.addEventListener("click", function () {
    let loan = prompt("How much would you like to loan?", 0);
    while (isNaN(loan)) {
        loan = prompt("Please enter a number", 0);
    }

    if (parseInt(loan) > 2 * fromNOKToNumber(bankBalanceEl.textContent)) {
        alert("You are unable to loan more than double of your current balance")
    }

    else if (fromNOKToNumber(outstandingLoanEl.textContent) > 0) {
        alert("You have to pay back your current loan to get a new one (or refresh the page)")
    }

    else {
        outstandingLoanEl.innerText = converter.format(loan);
        bankBalanceEl.textContent = converter.format(fromNOKToNumber(bankBalanceEl.textContent) + parseInt(loan));
        salaryToLoanButtonEl.removeAttribute("hidden");

    }
})

//Handles payment for work
salaryButtonEl.addEventListener("click", () => {
    let salary = (fromNOKToNumber(payEl.textContent));
    payEl.textContent = converter.format(salary + 100);

});

//Handles transfer from salary to balance
salaryToBankButtonEl.addEventListener("click", () => {
    let pay = fromNOKToNumber(payEl.textContent);
    let loan = fromNOKToNumber(outstandingLoanEl.textContent);
    if (loan > 0) {
        let partOfSalary = pay * 0.1
        outstandingLoanEl.textContent = converter.format(Math.max(loan - partOfSalary, 0));
        bankBalanceEl.textContent = converter.format(fromNOKToNumber(bankBalanceEl.textContent) + (pay - partOfSalary));
    }
    else {
        bankBalanceEl.textContent = converter.format(fromNOKToNumber(bankBalanceEl.textContent) + pay);
    }
    payEl.innerText = converter.format(0);

    if (fromNOKToNumber(outstandingLoanEl.textContent) === parseInt(0)) {
        salaryToLoanButtonEl.setAttribute("hidden", "hidden");
    }
});
//Handles payment of loan
salaryToLoanButtonEl.addEventListener("click", () => {
    let salary = fromNOKToNumber(payEl.textContent);
    let outstandingLoan = fromNOKToNumber(outstandingLoanEl.textContent);

    if (outstandingLoan - salary <= 0) {
        outstandingLoanEl.innerText = 0;
        bankBalanceEl.textContent = converter.format(fromNOKToNumber(bankBalanceEl.textContent) + salary - outstandingLoan);
        salaryToLoanButtonEl.setAttribute("hidden", "hidden");
    }
    else {
        outstandingLoanEl.innerText = converter.format(outstandingLoan - salary);
    }
    payEl.innerText = converter.format(0);
});


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(res => res.json())
    .then((products) => {
        laptops.push(...products);
        renderLaptops(laptops);
    });

//Adds laptops to HTML and sets the first object to selected
function renderLaptops(laptops) {
    for (const laptop of laptops) {
        const html = `<option value=${laptop.title} selected>${laptop.title}</option>`;
        laptopSelectorEl.insertAdjacentHTML("beforeend", html);
    }
    laptopSelectorEl.selectedIndex = 0;
    laptopDescriptionEl.innerText = laptops[0].specs;
    laptopImageEl.setAttribute("src", "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image);
    laptopImageEl.setAttribute("alt", laptops[0].title);
    laptopInformationDescriptionEl.innerText = laptops[0].description;
    laptopInformationTitleEl.innerText = laptops[0].title;
    laptopPriceEl.innerText = converter.format(laptops[0].price);

};
// handles the change of laptops
laptopSelectorEl.onchange = (e) => {
    let index = e.target.options.selectedIndex;
    laptopDescriptionEl.innerText = laptops[index].specs;
    laptopImageEl.setAttribute("src", "https://noroff-komputer-store-api.herokuapp.com/" + laptops[index].image);
    laptopImageEl.setAttribute("alt", laptops[index].title);
    laptopInformationDescriptionEl.innerText = laptops[index].description;
    laptopInformationTitleEl.innerText = laptops[index].title;
    laptopPriceEl.innerText = converter.format(laptops[index].price);
}
//handles the buy laptop logic.
buyButtomEl.addEventListener("click", () => {
    let balance = fromNOKToNumber(bankBalanceEl.textContent);
    let index = laptopSelectorEl.selectedIndex;

    if (balance >= parseInt(laptops[index].price)) {
        bankBalanceEl.innerText = converter.format(balance - laptops[index].price);
        alert("Congratulation! You are the owner of a new laptop (for now...)");
    }
    else {
        alert("Sorry, you can not afford this laptop.");
    }
});









